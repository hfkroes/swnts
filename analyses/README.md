# Analysis Modules

This directory contains various analysis modules in the Schwannomatosis Open Research Collaborative. See the README in an individual analysis' directory for more information about that analysis.

### Analyses at a glance

The table below is intended to help project organizers quickly get an idea of what files (and by extension types of data) are consumed by each analysis, what the analysis does, and what outputs the analysis produces.


| Analysis | Input Files | Brief Description | Output Files |
|--------|-------|-------------------|-------|
| variant-caller-comparison | WES vcfs | Ongoing. Get high confidence calls by comparing vcfs | Summary/QC plots |
| differential-expression | RNA-seq gene quantitation from Salmon | Ongoing. Identify differentially expressed genes between Schwannoma samples and normal tibial nerve samples from GTEx. | QC plots, list of consensus differentially expressed genes |
