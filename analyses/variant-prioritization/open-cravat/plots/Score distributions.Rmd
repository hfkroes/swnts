---
title: "Score Distributions"
author: "Adon"
date: "4/17/2023"
output: 
  html_document:
    toc: true
    toc_float: true
    toc_collapsed: true
    toc_depth: 3
    number_sections: false
    code_folding: hide
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE,
                      message=FALSE,warning=FALSE)
```


```{r}
library(dplyr)
library(readr)
library(ggplot2)
library(gridExtra)
library(cowplot)
library(stringr)
library(extrafont)
library(sagethemes)
```

## Read Data
```{r}
aloft_scores<-read_delim("aloft_scores1.csv",delim = ";", escape_double = FALSE, trim_ws = TRUE)
cadd_scores<-read_delim("cadd_scores1.csv", delim = ";", escape_double = FALSE, trim_ws = TRUE)
provean_scores <- read_delim("provean_scores1.csv",delim = ";", escape_double = FALSE, trim_ws = TRUE)
mutpred1_scores <- read_delim("mutpred1_scores1.csv",delim = ";", escape_double = FALSE, trim_ws = TRUE)
mutationtaster_scores <-read_delim("OC_mutationtaster_scores1.csv", delim = ";", escape_double = FALSE, trim_ws = TRUE)
dbscsnv_scores<-read_delim("OC_dbscsnv_scores1.csv", delim = ";", escape_double = FALSE, trim_ws = TRUE)
```

## aloft plots
```{r}
aloft_scores<-aloft_scores%>%
  mutate(Disease_causing = if_else(aloft__pred %in% c("Recessive","Dominant"), "TRUE","FALSE"))

counts <- aloft_scores%>%
  group_by(Disease_causing,aloft__conf)%>%
  summarise(variant_count =n())

ggplot(counts, aes(fill=Disease_causing, y= variant_count, x =aloft__conf ))+
  geom_bar(stat = "identity")+
  labs(x = "Aloft confidence", y = "Variant count", fill = "Disease causing variant") +
  scale_fill_manual(values = c("#92c5de", "#0571b0"))+
  theme_sage()
```

## CADD plots

```{r}
regular_score_plot<-ggplot(cadd_scores, aes(x=cadd_exome__score))+
    geom_histogram(binwidth = 0.1, color = "white", fill = "#0571b0") +
    labs(x = "CADD score (Regular)", y = "Count")+
    theme_sage()
scaled_score_plot<-ggplot(cadd_scores, aes(x=cadd_exome__phred))+
  geom_histogram(binwidth = 0.5, color = "white", fill = "#0571b0") +
  labs(x = "CADD score (Phred)", y = "Count")+
  theme_sage()
combined_plots<-grid.arrange(regular_score_plot, scaled_score_plot, ncol = 2)

cadd_plot_title<-ggdraw() +
  draw_label("Distribution of CADD exome scores and phred scores", fontface = "bold", x = 0.3, hjust = 0, color = "#0571b0")
final_plot <- plot_grid(cadd_plot_title, combined_plots, ncol = 1, align = "v", rel_heights = c(0.1, 1))
final_plot 
```

## mutpred plots

```{r}
mutpred1_scores<-mutpred1_scores%>%
  mutate(mutpred1__mutpred_general_score = gsub(",","",mutpred1__mutpred_general_score),
         mutpred1__mutpred_rankscore = gsub(",","",mutpred1__mutpred_rankscore))%>%
  mutate(mutpred1__mutpred_general_score = as.numeric(mutpred1__mutpred_general_score),
         mutpred1__mutpred_rankscore = as.numeric(mutpred1__mutpred_rankscore))
mutpred_regular_score_plot<-ggplot(mutpred1_scores, aes(x=mutpred1__mutpred_general_score))+
  geom_histogram(binwidth = 0.1, color = "white", fill = "#0571b0") +
  labs(x = "Mutpred score (general)", y = "Count")+
  theme_sage()
mutpred_rank_score_plot<-ggplot(mutpred1_scores, aes(x=mutpred1__mutpred_rankscore))+
  geom_histogram(binwidth = 0.1, color = "white", fill = "#0571b0") +
  labs(x = "Mutpred score (Rank)", y = "Count")+
  theme_sage()
mutpred_combined_plots<-grid.arrange(mutpred_regular_score_plot, mutpred_rank_score_plot, ncol = 2)

mutpred_plot_title<-ggdraw() +
  draw_label("Distribution of Mutpred scores and Rank scores", fontface = "bold", x = 0.3, hjust = 0, color = "#0571b0")
mutpred_final_plot <- plot_grid(mutpred_plot_title, mutpred_combined_plots, ncol = 1, align = "v", rel_heights = c(0.1, 1))
mutpred_final_plot
```

## mutation tester plots

```{r}
mutationtaster_score_plot<-ggplot(mutationtaster_scores, aes(x=mutationtaster__score))+
  geom_histogram(binwidth = 0.1, color = "white", fill = "#0571b0") +
  labs(x = "Mutation tsater score (general)", y = "Count")+
  geom_vline(xintercept = 0.5, colour = "red")+
  theme_sage()
mutationtaster_rank_score_plot<-ggplot(mutationtaster_scores, aes(x=mutationtaster__rankscore))+
  geom_histogram(binwidth = 0.1, color = "white", fill = "#0571b0") +
  labs(x = "Mutation taster score (Rank)", y = "Count")+
  geom_vline(xintercept = 0.5, colour = "red")+
  theme_sage()
mutationtaster_combined_plots<-grid.arrange(mutationtaster_score_plot, mutationtaster_rank_score_plot, ncol = 2)

mutationtaster_plot_title<-ggdraw() +
  draw_label("Distribution of Mutationtaster scores and Rank scores", fontface = "bold", x = 0.3, hjust = 0, color = "#0571b0")
mutationtater_final_plot <- plot_grid(mutationtaster_plot_title, mutationtaster_combined_plots, ncol = 1, align = "v", rel_heights = c(0.1, 1))
mutationtater_final_plot
```

## provean plots

```{r}
provean_scores<-provean_scores%>%
  filter(!grepl(",", provean__score))%>%
  mutate(provean__score = as.numeric(provean__score),
         provean__rankscore = as.numeric(provean__rankscore))

provean_score_dist_plot <- ggplot(provean_scores, aes(x=provean__score))+
  geom_histogram(binwidth = 0.1, color = "white", fill = "#0571b0")+
  labs(x = "Provean Score", y="Count", title = "Distribution of Provean Scores")+
  theme_sage()
provean_rank_score_dist_plot <- ggplot(provean_scores, aes(x=provean__rankscore))+
  geom_histogram(binwidth = 0.1, color = "white", fill = "#0571b0")+
  labs(x = "Provean Score(Rank)", y="Count", title = "Distribution of Provean (Rank) Scores")+
  theme_sage()

provean_scatter_plot<-ggplot(provean_scores, aes(x =provean__score, y=provean__prediction, color = provean__prediction))+
  geom_boxplot()+
  #scale_color_manual(values = c("blue", "red"), labels = c("Neutral", "Damaging"))+
  labs(x = "Provean Score", y = "Predicted Effect", title = "Provean Score vs. Predicted Effect")+
  theme_sage()

damaging_provean_scores<-provean_scores%>%
  filter(provean__prediction == "Damaging")%>%
  ggplot(aes(x=provean__score))+
  geom_histogram(binwidth = 0.1, color = "white", fill = "light blue")+
  theme_sage()
provean_scatter_plot
```
