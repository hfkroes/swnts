# Purpose and Implementation

#### What scientific question does your analysis address?

#### What was your approach?

#### What GitLab issue does your merge request address?

# Results

#### What types of results are included (e.g. tables, figures, etc.)?

#### Please provide a summary of your results

# Reviewer Instructions

#### Which areas should receive a particularly close look?

#### Is there anything that you want to discuss further?

#### Is the analysis in a mature enough form that the resulting figure(s) and/or table(s) are ready for review?

# Reproducibility

- [ ] The dependencies required to run the code in this merge request have been added to the project Dockerfile.
- [ ] This analysis has been added to continuous integration.

# Documentation

- [ ] This analysis module has a `README` and it is up to date.
- [ ] This analysis is recorded in the table in `analyses/README.md` and the entry is up to date.
- [ ] The analytical code is documented and contains comments.

<!-- IF YOUR MERGE REQUEST IS A DATA RELEASE, PLEASE REMOVE THE [HTML COMMENT TAG](https://html.com/tags/comment-tag/) FROM THE SECTION BELOW AND COMPLETE THE CHECKLIST-->

<!-- 
### Data Release Checklist
- [ ] Is the table in doc/data-file-descriptions.md up to date?
- [ ] Is doc/data-format.md up to date?
- [ ] Is doc/release-notes.md up to date?
- [ ] Is download-data.sh up to date?
- [ ] Was download-data.sh tested and did it complete without error?
-->
